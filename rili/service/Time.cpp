#include <algorithm>
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <rili/MakeUnique.hpp>
#include <rili/service/Time.hpp>
#include <thread>

namespace rili {
namespace service {

class Time::Impl {
 private:
    typedef std::function<void()> Callback;
    typedef std::function<void()> Cancel;
    typedef std::uint_least64_t TimerId;

 private:
    Cancel deferImplementation(Clock::time_point const& timePoint, Callback const& cb) noexcept;
    void worker();

 public:
    Impl();
    CancelablePromise<void> promiseUntil(Clock::time_point const& timePoint) noexcept;
    ~Impl();

    struct Invoke {
        Invoke(TimerId, Context&, Callback const&);

        TimerId timerId;
        Context& context;
        Callback timeoutHandler;
    };

 public:
    typedef std::multimap<Clock::time_point, Invoke> Timeouts;
    Timeouts m_timeouts;
    std::mutex m_mutex;
    std::condition_variable m_cv;
    bool m_stop;
    std::thread m_thread;
    TimerId m_counter;
};

CancelablePromise<void> Time::promiseUntil(Clock::time_point const& timePoint) noexcept {
    return m_impl->promiseUntil(timePoint);
}

Time::Time() : m_impl(rili::make_unique<Impl>()) {}
Time& Time::get() noexcept {
    static Time time;
    return time;
}

Time::Impl::Invoke::Invoke(TimerId t, Context& c, Time::Impl::Callback const& h)
    : timerId(t), context(c), timeoutHandler(h) {}

CancelablePromise<void> Time::Impl::promiseUntil(Clock::time_point const& timePoint) noexcept {
    return CancelablePromise<void>([this, timePoint](CancelablePromise<void>::OnResolveHandler const& resolve,
                                                     CancelablePromise<void>::OnCancel const& onCancel) {
        onCancel(this->deferImplementation(timePoint, resolve));
    });
}

Time::Impl::~Impl() {
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_stop = true;
        m_cv.notify_one();
    }
    m_thread.join();
}

Time::Impl::Cancel Time::Impl::deferImplementation(Clock::time_point const& timePoint, Callback const& cb) noexcept {
    TimerId timerId;
    auto& ctx = Context::get();
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        timerId = (++m_counter);
        m_timeouts.emplace(std::piecewise_construct, std::forward_as_tuple(timePoint),
                           std::forward_as_tuple(timerId, ctx, cb));
        m_cv.notify_one();
    }

    return [this, &ctx, timerId]() {
        ctx.schedule([this, timerId] {
            {
                std::lock_guard<std::mutex> lock(m_mutex);
                const auto found =
                    std::find_if(m_timeouts.begin(), m_timeouts.end(),
                                 [timerId](Timeouts::value_type const& v) { return v.second.timerId == timerId; });
                if (found != m_timeouts.end()) {
                    m_timeouts.erase(found);
                    m_cv.notify_one();
                }
            }
        });
    };
}

void Time::Impl::worker() {
    for (;;) {
        std::unique_ptr<Invoke> invoke;
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_stop) {
                return;
            } else if (m_timeouts.empty()) {
                m_cv.wait(lock);
            } else if (Clock::now() >= m_timeouts.cbegin()->first) {
                invoke = rili::make_unique<Invoke>(m_timeouts.begin()->second);
                m_timeouts.erase(m_timeouts.begin());
            } else {
                m_cv.wait_until(lock, m_timeouts.cbegin()->first);
            }
        }
        if (invoke) {
            invoke->context.schedule(invoke->timeoutHandler);
        }
    }
}

Time::Impl::Impl() : m_timeouts(), m_mutex(), m_cv(), m_stop(false), m_thread(&Impl::worker, this), m_counter(0) {
    std::lock_guard<std::mutex> lock(m_mutex);
}

}  // namespace service
}  // namespace rili
