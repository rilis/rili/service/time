#pragma once
/** @file
 */

#include <chrono>
#include <memory>
#include <rili/CancelablePromise.hpp>

namespace rili {
namespace service {

/**
 * @brief The TimeBase class is interface class for Time service which give ability to create promises which will be
 * resolved
 * after some time.
 *
 * It can be used for example as timeouts implementation. Derivering form this class could be usefull if you wnat
 * provide own implementation related to timer management or to create mocks or stubs for testing purposes.
 *
 * @remark is abstract class
 * @remark use steady clock
 *
 * @see rili::service::Time
 * @see rili::Promise
 */
class TimeBase {
 public:
    /**
     * @brief define type of clock used by Time related services.
     */
    typedef std::chrono::steady_clock Clock;
    virtual ~TimeBase() = default;

    /**
     * @brief This function return rili::CancelablePromise<void> which will be resolved when time(from `duration`)
     * elapse.
     *
     * If promise will be canceled resources related to timer will be released. Implementation of this method depends on
     * implementation of promiseUntil.
     *
     * @remark if promise will be canceled resources related to timer will be released.
     * @remark is thread safe.
     *
     * @see rili::CancalablePromise<void>
     * @see promiseUntil
     *
     * @param duration points how much time should elapse until returned promise will be resolved
     * @return rili::CancelablePromise<void> which will be resolved after given in duration time
     */
    template <class Rep, class Period>
    CancelablePromise<void> promiseFor(std::chrono::duration<Rep, Period> const& duration) noexcept {
        return promiseUntil(Clock::now() + duration);
    }

    /**
     * @brief promiseUntil is pure virtual method intended to be interface for Time related services.
     *
     * Derived class should provide implementation, which will resolved returned promise exactly at given time point ar
     * as soon as possible after it.
     *
     * @note It is expected that implementation of this method in derived should be thread safe.
     *
     * @param timePoint defines moment after which returned promise should be resolved.
     * @return rili::CancelablePromise<void> which will be resolved in given timePoint.
     *
     * @see promiseFor
     */
    virtual CancelablePromise<void> promiseUntil(Clock::time_point const& timePoint) noexcept = 0;
};
/**
 * @brief The Time class is default TimeBase service implementation.
 *
 * Give ability to create promises which will be resolved after some time. This can be used for example as timeouts
 * implementation.
 *
 * @remark is non-copyable
 * @note create internally single thread which manage all timers
 *
 * @see rili::service::TimeBase
 * @see rili::CancalablePromise<void>
 */
class Time final : public TimeBase {
 public:
    /**
     * @brief This function return rili::CancelablePromise<void> which will be resolved as soon as possible after
     * timePoint time moment will be achived.
     *
     * @remark if promise will be canceled resources related to timer will be released.
     * @remark is thread safe
     *
     * @param timePoint defines moment after which returned promise should be resolved.
     * @return rili::CancelablePromise<void> which will be resolved in given timePoint.
     *
     * @see promiseFor()
     */
    CancelablePromise<void> promiseUntil(Clock::time_point const& timePoint) noexcept override;

    /**
     * @brief Get or create rili::service::Time instance.
     *
     * New rili::service::Time is created if was not created earlier. If rili::service::Time was created previously
     * return existing instance.
     * @return Instance of rili::service::Time
     */
    static Time& get() noexcept;

 private:
    class Impl;
    std::unique_ptr<Impl> m_impl;
    Time();
    Time(Time const& other) = delete;
    Time& operator=(Time const&) = delete;
};

}  // namespace service
}  // namespace rili
