# Rili Time service

This is module of [rili](https://gitlab.com/rili) which provide service responsible for timers management.

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/service/time)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/service/time/badges/master/build.svg)](https://gitlab.com/rilis/rili/service/time/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/service/time/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/service/time/commits/master)
