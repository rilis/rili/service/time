#include <memory>
#include <rili/Test.hpp>
#include <rili/service/Time.hpp>

TEST(Service_Time, Until) {
    rili::Context::run([]() {
        typedef rili::service::Time::Clock Clock;
        auto expectedTp(Clock::now() + std::chrono::milliseconds(10 * TIME_SCALE_FACTOR));
        rili::service::Time::get().promiseUntil(expectedTp).Then([expectedTp]() {
            auto now(Clock::now());
            EXPECT_GE(now, expectedTp);
            EXPECT_LE(now, expectedTp + std::chrono::milliseconds(20 * TIME_SCALE_FACTOR));
        });
    });
}

TEST(Service_Time, For) {
    rili::Context::run([]() {
        typedef rili::service::Time::Clock Clock;
        auto expectedTp(Clock::now() + std::chrono::milliseconds(10 * TIME_SCALE_FACTOR));
        rili::service::Time::get().promiseFor(std::chrono::milliseconds(10 * TIME_SCALE_FACTOR)).Then([expectedTp]() {
            auto now(Clock::now());
            EXPECT_GE(now, expectedTp);
            EXPECT_LE(now, expectedTp + std::chrono::milliseconds(20 * TIME_SCALE_FACTOR));
        });
    });
}

TEST(Service_Time, CancelAfterTimeout) {
    rili::Context::run([]() {
        auto timeouted = std::make_shared<bool>(false);
        auto cancelable = rili::service::Time::get().promiseFor(std::chrono::milliseconds(10 * TIME_SCALE_FACTOR));
        cancelable.Then([timeouted]() { *timeouted = true; }, [](rili::CancelablePromise<void>::FailureType const&) {});

        rili::service::Time::get()
            .promiseFor(std::chrono::milliseconds(20 * TIME_SCALE_FACTOR))
            .Then([cancelable]() mutable { cancelable.cancel(); })
            .Then([timeouted]() { EXPECT_TRUE(*timeouted); });
    });
}

TEST(Service_Time, CancelBeforeTimeout) {
    rili::Context::run([]() {
        auto timeouted = std::make_shared<bool>(false);

        auto cancelable = rili::service::Time::get().promiseFor(std::chrono::milliseconds(20 * TIME_SCALE_FACTOR));

        cancelable.Then([timeouted]() { *timeouted = true; }, [](rili::CancelablePromise<void>::FailureType const&) {});

        rili::service::Time::get()
            .promiseFor(std::chrono::milliseconds(10 * TIME_SCALE_FACTOR))
            .Then([cancelable]() mutable { cancelable.cancel(); })
            .Then([]() -> rili::Promise<void> {
                return rili::service::Time::get().promiseFor(std::chrono::milliseconds(40 * TIME_SCALE_FACTOR));
            })
            .Then([timeouted]() { EXPECT_FALSE(*timeouted); });
    });
}
